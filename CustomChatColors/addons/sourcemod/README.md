To add default tag for certain flags, do those commands:

sm_cccaddtag Admins 1 Admins b "[ADMIN] " FF4040 3EFF3E 00FFFF
sm_cccaddtag VIP 1 VIP o "[VIP] " D147FF "" ""

To add the replace triggers from the cfg given in this repository, simply run

sm_cccimportreplacefile custom-chatcolorsreplace.cfg
