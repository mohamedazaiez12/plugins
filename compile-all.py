#!/usr/bin/python3
import os
import sys
import subprocess

c_null  = "\x1b[00;00m"
c_red   = "\x1b[31;01m"
c_blue  = "\x1b[34;01m"
c_green = "\x1b[32;01m"

SM_INCLUDES = "includes"
SPCOMP = "./spcomp"

if __name__ == "__main__":
	Plugins = []
	Path, Directories, Files = next(os.walk("."))
	for Directory in Directories:
		if not Directory.startswith(".") and not Directory.startswith("_") and Directory != "include" and Directory != "includes" and Directory != "plugins":
			Plugins.append(Directory)

	for Plugin in Plugins:
		print(c_red + "### Compiling {0}".format(Plugin) + c_null)

		SourcePath = os.path.join(Plugin, "addons/sourcemod/scripting")
		try:
			Path, Directories, Files = next(os.walk(SourcePath))
		except StopIteration:
			continue
		for File in Files:
			if File.endswith(".sp"):
				print(c_green + "# Compiling {0} ({1})".format(os.path.basename(File), Plugin) + c_null)
				SourcePath = os.path.join(Path, File)
				IncludePath = os.path.join(Path, "include")
				OutDir = "plugins"
				OutPath = os.path.join(OutDir, os.path.splitext(os.path.basename(SourcePath))[0] + ".smx")

				Compiler = [SPCOMP, "-i" + SM_INCLUDES, "-i" + "include"]
				if os.path.isdir(IncludePath):
					Compiler.append("-i" + IncludePath)
				Compiler.append(SourcePath)
				Compiler.append("-o" + OutPath)

				try:
					err = subprocess.call(Compiler)
					if err:
						raise Exception()
				except Exception:
					sys.exit(1)

				print("")
