#pragma semicolon 1

#define PLUGIN_AUTHOR "null138"
#define PLUGIN_VERSION "1.00"

#include <sourcemod>
#include <sdktools>

#pragma newdecls required

int 
	iGameFrame = 1,
	iFakeLaser = -1;

public Plugin myinfo = 
{
	name = "[MG-ZE] Laser Hack Fix",
	author = PLUGIN_AUTHOR,
	description = "Fixes laser hacks",
	version = PLUGIN_VERSION,
	url = "https://steamcommunity.com/id/null138/"
}

public void OnMapStart()
{
	iFakeLaser = -1;
	CreateFakeLaser();
}

public void OnGameFrame()
{
	if(iGameFrame < 1 || iGameFrame > MaxClients)
	{
		iGameFrame = 1;
	}

	int fakeLaser = EntRefToEntIndex(iFakeLaser);
	
	if(IsClientInGame(iGameFrame) && IsPlayerAlive(iGameFrame))
	{
		if(!IsValidEntity(fakeLaser))
		{
			CreateFakeLaser();
			return;
		}
		float pos[3];
		GetClientAbsOrigin(iGameFrame, pos);
		pos[2] += GetRandomFloat(30.0, 60.0);

		TeleportEntity(fakeLaser, pos, NULL_VECTOR, NULL_VECTOR);
	}
	iGameFrame++;
}

void CreateFakeLaser()
{
	int fakeLaser = CreateEntityByName("func_movelinear");
	if(fakeLaser == -1) return;
	
	iFakeLaser = EntIndexToEntRef(fakeLaser);

	DispatchKeyValue(fakeLaser, "rendermode", "3");
	DispatchKeyValue(fakeLaser, "renderamt", "255");
	DispatchKeyValue(fakeLaser, "spawnflags", "8");
	DispatchKeyValue(fakeLaser, "movedir", "0 270 0");
	DispatchKeyValue(fakeLaser, "startposition", "0");
	DispatchKeyValue(fakeLaser, "speed", "300");
	DispatchKeyValue(fakeLaser, "movedistance", "5000000000000000000");
	DispatchKeyValue(fakeLaser, "blockdamage", "0");
	DispatchKeyValue(fakeLaser, "rendercolor", "26 0 255");

	DispatchSpawn(fakeLaser);
	ActivateEntity(fakeLaser);

	TeleportEntity(fakeLaser, view_as<float>({0.0, 0.0, 0.0}), NULL_VECTOR, NULL_VECTOR);
	
	PrecacheModel("models/props/cs_office/vending_machine.mdl");
	SetEntityModel(fakeLaser, "models/props/cs_office/vending_machine.mdl");

	SetEntPropVector(fakeLaser, Prop_Send, "m_vecMins", view_as<float>({-145.0, -342.0, -8.0}));
	SetEntPropVector(fakeLaser, Prop_Send, "m_vecMaxs", view_as<float>({145.3, 342.0, 8.0}));
    
	int effects = GetEntProp(fakeLaser, Prop_Send, "m_fEffects");
	effects |= 32;
	SetEntProp(fakeLaser, Prop_Send, "m_fEffects", effects);

	AcceptEntityInput(fakeLaser, "Open");
}