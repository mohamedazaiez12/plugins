#include <sdktools_sound>

#undef REQUIRE_PLUGIN
#tryinclude <materialadmin>
#tryinclude <sourcebanspp>

bool g_bBanClient;
#if defined _materialadmin_included
bool g_bMaterialAdmin;
#endif
#if defined _sourcebans_included
bool g_bSourceBans;
#endif
int g_iConnectNetMsgCount[MAXPLAYERS + 1];
char g_szLog[PLATFORM_MAX_PATH];

public Plugin myinfo =
{
	name		= "NullWave Crash Fix",
	author		= "backwards, IT-KiLLER, SM9();, Roy (Christian Deacon)",
	description	= "Exploit Fix for [SB] & [MA]",
	version		= "0.2.2"
}

public void OnPluginStart()
{
	if (GetEngineVersion() != Engine_CSGO)
		SetFailState("[NullWaveFix] This game is not supported");

	HookEvent("player_connect_full", Event_PlayerConnectFull, EventHookMode_Pre);
	AddNormalSoundHook(NormalSoundHook);
	AddAmbientSoundHook(AmbientSoundHook);

	BuildPath(Path_SM, g_szLog, sizeof(g_szLog), "logs/NullWaveCrashFix.log");

	ConVar CVar;
	(CVar = CreateConVar("sm_nwfix_ban", "1", "Should the client be banned for crash attempt?", FCVAR_PROTECTED, true, _, true, 1.0)).AddChangeHook(CVarChanged_Ban);
	g_bBanClient = CVar.BoolValue;
#if defined _sourcebans_included
	g_bSourceBans = LibraryExists("sourcebans++");
#endif
#if defined _materialadmin_included
	g_bMaterialAdmin = LibraryExists("materialadmin");
#endif

	AutoExecConfig(true);
}

public void CVarChanged_Ban(ConVar CVar, const char[] oldValue, const char[] newValue)
{
	g_bBanClient = CVar.BoolValue;
}

public void OnLibraryAdded(const char[] szName)
{
#if defined _sourcebans_included
	if (StrEqual(szName, "sourcebans++"))
		g_bSourceBans = true;
#endif
#if defined _materialadmin_included
	if (StrEqual(szName, "materialadmin"))
		g_bMaterialAdmin = true;
#endif
}

public void OnLibraryRemoved(const char[] szName)
{
#if defined _sourcebans_included
	if (StrEqual(szName, "sourcebans++"))
		g_bSourceBans = false;
#endif
#if defined _materialadmin_included
	if (StrEqual(szName, "materialadmin"))
		g_bMaterialAdmin = false;
#endif
}

public void OnMapStart()
{
	for (int i = 1; i <= MaxClients; i++) g_iConnectNetMsgCount[i] = 0;
}

public void OnClientDisconnect(int client)
{
	g_iConnectNetMsgCount[client] = 0;
}

public Action Event_PlayerConnectFull(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));

	if (client && !IsClientInGame(client) && IsClientConnected(client) && !IsFakeClient(client) && !IsClientSourceTV(client) && !IsClientReplay(client))
	{
		if (!IsClientInKickQueue(client))
		{
			KickClient(client, "Something went wrong, please retry connecting");
			LogToFileEx(g_szLog, "Kicked %L for sending an early player_connect_full event (Possible crash attempt)", client);
		}
		event.BroadcastDisabled = true;
		return Plugin_Changed;
	}

	if (++g_iConnectNetMsgCount[client] == 1)
		return Plugin_Continue;

	if (!IsClientInKickQueue(client))
	{
		LogToFileEx(g_szLog, "%s %L for sending more than one player_connect_full event (Confirmed crash attempt)", g_bBanClient ? "Banned" : "Kicked", client);
		if(g_bBanClient) BanCrasher(client);
		else KickClient(client, "Attempted server crash exploit");
	}

	event.BroadcastDisabled = true;
	return Plugin_Changed;
}

stock void BanCrasher(int client)
{
#if defined _materialadmin_included
	if(g_bMaterialAdmin)
	{
		MABanPlayer(0, client, MA_BAN_STEAM, 0, "Attempted server crash exploit");
		return;
	}
#endif
#if defined _sourcebans_included
	if(g_bSourceBans)
	{
		SBPP_BanPlayer(0, client, 1200, "Attempted server crash exploit");
		return;
	}
#endif
	BanClient(client, 0, BANFLAG_AUTO, "Attempted server crash exploit", "Attempted server crash exploit");
}

public Action NormalSoundHook(int clients[64], int &numClients, char sample[PLATFORM_MAX_PATH], int &entity, int &channel, float &volume, int &level, int &pitch, int &flags)
{
	return IsNullWav(sample) ? Plugin_Stop : Plugin_Continue;
}

public Action AmbientSoundHook(char sample[PLATFORM_MAX_PATH], int &entity, float &volume, int &level, int &pitch, float pos[3], int &flags, float &delay)
{
	return IsNullWav(sample) ? Plugin_Stop : Plugin_Continue;
}

stock bool IsNullWav(char[] name)
{
	return !name[0] || name[0] == 'c' && (StrEqual(name, "common\null.wav") || StrEqual(name, "common/null.wav"));
}