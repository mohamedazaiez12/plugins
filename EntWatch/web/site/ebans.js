function DoLogin(redir)
{
	var err = 0;
	if(!$('#loginUsername').val())
	{
		$('#loginUsernameMsg').html('You must enter your loginname!');
		$('#loginUsernameMsg').css('display', 'block');
		err++;
	}else
	{
		$('#loginUsernameMsg').html('');
		$('#loginUsernameMsg').css('display', 'none');
	}

	if(!$('#loginPassword').val())
	{
		$('#loginPasswordMsg').html('You must enter your password!');
		$('#loginPasswordMsg').css('display', 'block');
		err++;
	}else
	{
		$('#loginPasswordMsg').html('');
		$('#loginPasswordMsg').css('display', 'none');
	}

	if(err)
		return 0;

	if(redir == "undefined")
		redir = "";

	xajax_Plogin(document.getElementById('loginUsername').value,
				document.getElementById('loginPassword').value,
				 document.getElementById('loginRememberMe').checked,
				 redir);
}

function DeleteBan(ban_id)
{
	xajax_DeleteBan(ban_id);
}