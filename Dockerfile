FROM registry.gitlab.com/counterstrikesource/sourcemodexts:latest

# Author
LABEL maintainer="maxime1907 <maxime1907.dev@gmail.com>"

RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting

WORKDIR /home/git/

# Copy every sourcepawn and include file that we need
RUN git clone https://gitlab.com/counterstrikesource/hlstatsx.git --recursive
RUN git clone https://gitlab.com/counterstrikesource/zombiereloaded.git --recursive

# Copy to corresponding folders
COPY . /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting

# ZombieReloaded
WORKDIR /home/git/zombiereloaded/

RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/zombiereloaded/addons/sourcemod/scripting/
RUN bash /home/git/zombiereloaded/updateversion.sh --unofficial
RUN cp /home/git/zombiereloaded/src/* -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/zombiereloaded/addons/sourcemod/scripting/
RUN cp /home/git/zombiereloaded/cstrike/* -R /home/alliedmodders/sourcemod/build/package_plugins/

WORKDIR /home/git/

# Hlstatsx
RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/hlstatsx/addons/sourcemod/scripting/
RUN cp -R /home/git/hlstatsx/sourcemod/scripting/include/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/include/
RUN cp /home/git/hlstatsx/sourcemod/scripting/hlstatsx.sp /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/hlstatsx/addons/sourcemod/scripting/
RUN cp /home/git/hlstatsx/sourcemod/scripting/superlogs-css.sp /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/hlstatsx/addons/sourcemod/scripting/

# Copy include files
RUN cp -R /home/alliedmodders/sourcemod/build/package_extensions/addons/sourcemod/scripting/include/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/include/
RUN cp -R /home/alliedmodders/sourcemod/build/package/addons/sourcemod/scripting/include/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/include/
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/*/addons/sourcemod/scripting/include/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/include/
RUN cp /home/alliedmodders/sourcemod/build/package/addons/sourcemod/scripting/spcomp /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/

# Remove useless files
RUN rm -Rf /home/git/

# Compile plugins
WORKDIR /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/

RUN python3 compile-all.py

# Prepare output directories
RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/configs/
RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/data/
RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/gamedata/
RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/translations/
RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/plugins/custom/

# Copy plugins data, configs and .smx
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/plugins/custom/
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/*/addons/sourcemod/configs/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/configs/
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/*/addons/sourcemod/data/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/data/
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/*/addons/sourcemod/gamedata/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/gamedata/
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/*/addons/sourcemod/translations/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/translations/
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/*/materials/* /home/alliedmodders/sourcemod/build/package_plugins/materials/
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/*/models/* /home/alliedmodders/sourcemod/build/package_plugins/models/
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/*/sound/* /home/alliedmodders/sourcemod/build/package_plugins/sound/
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/*/cfg/* /home/alliedmodders/sourcemod/build/package_plugins/cfg/
